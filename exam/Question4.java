package exam;


import java.util.Scanner;
  
public class Question4
{
   public static void main(String args[])
   {
      int n;
      int c;
      System.out.println("Enter an integer to print it's multiplication table");
      Scanner in = new Scanner(System.in);
      n = in.nextInt();
      System.out.println("Multiplication table of "+n+" is :-");
  
      for ( c = 1 ; c <= 16 ; c++ )
         System.out.println(n+"*"+c+" = "+(n*c));
   }
}